Liones AngularJS Performance Presentation
=========================================

These are the slides for a presentation given at the [SupersonicCookie 
Dutch AngularJS meetup](https://plus.google.com/communities/103800317107611206686), on 2014/03/04, by Stef Busking and Bert Willems.

[https://bitbucket.org/liones/liones-angularjs-performance-presentation](https://bitbucket.org/liones/liones-angularjs-performance-presentation)

In this presentation we present lessons learned during the implementation 
of a large, AngularJS-based application.
